module amr_parameters

  ! Define real types
  integer,parameter::sp=kind(1.0E0)
#ifndef NPRE
  integer,parameter::dp=kind(1.0E0) ! default
#else
#if NPRE==4
  integer,parameter::dp=kind(1.0E0) ! real*4
#else
  integer,parameter::dp=kind(1.0D0) ! real*8
#endif
#endif
  integer,parameter::MAXOUT=1000
  integer,parameter::MAXLEVEL=65
  integer,parameter::MAXREGION=100
  
  ! Define integer types (for particle IDs mostly)
  integer,parameter::i4b=4
#ifndef LONGINT
  integer,parameter::i8b=4  ! default long int are short int
#else
  integer,parameter::i8b=8  ! long int are long int
#endif

  ! Number of dimensions
#ifndef NDIM
  integer,parameter::ndim=1
#else
  integer,parameter::ndim=NDIM
#endif
  integer,parameter::twotondim=2**ndim
  integer,parameter::threetondim=3**ndim
  integer,parameter::twondim=2*ndim

  ! Number of 64-bit integers needed to store one Hilbert key
#ifndef NHILBERT
  integer, parameter :: nhilbert = 1
#else
  integer, parameter :: nhilbert = NHILBERT
#endif

  ! Vectorization parameter
#ifndef NVECTOR
  integer,parameter::nvector=32  ! Size of vector sweeps
#else
  integer,parameter::nvector=NVECTOR
#endif

  ! Expansion factor look-up table size
  integer,parameter::n_frw=1000

  ! Useful constants
  real(kind=8),parameter ::twopi   = 6.2831853d0
  real(kind=8),parameter ::hplanck = 6.6262000d-27
  real(kind=8),parameter ::eV      = 1.6022000d-12
  real(kind=8),parameter ::kB      = 1.3806200d-16
  real(kind=8),parameter ::clight  = 2.9979250d+10
  real(kind=8),parameter ::Gyr     = 3.1536000d+16
  real(kind=8),parameter ::rhoc    = 1.8800000d-29
  real(kind=8),parameter ::mH      = 1.6600000d-24
  real(kind=8),parameter ::X_H     = 0.76 ! Hydrogen mass fraction
  real(kind=8),parameter ::Y_He    = 0.24 ! Helium mass fraction

  ! Executable identification
  CHARACTER(LEN=80)::builddate,patchdir
  CHARACTER(LEN=80)::gitrepo,gitbranch,githash

  ! Save namelist filename
  CHARACTER(LEN=80)::namelist_file

end module amr_parameters

